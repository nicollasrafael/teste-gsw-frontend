import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  listCustomers() : Observable<Customer[]> {
    return this.httpClient
      .get<Customer[]>(environment.apiBaseUrl + '/customer')
      .pipe(
        catchError(this.handleError)
      );
  }

  createCustomer(customerRequest: Customer) : Observable<Customer> {
    return this.httpClient
      .post<Customer>(environment.apiBaseUrl + '/customer', customerRequest)
      .pipe(
        catchError(this.handleError)
      );
  }

  getCustomer(customerId: number) : Observable<Customer> {
    return this.httpClient
      .get<Customer>(environment.apiBaseUrl + `/customer/${customerId}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateCustomer(customerId: number, customerRequest: Customer) {
    return this.httpClient
      .put(environment.apiBaseUrl + `/customer/${customerId}`, customerRequest)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteCustomer(customerId: number) {
    return this.httpClient
      .delete(environment.apiBaseUrl + `/customer/${customerId}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };
}

export interface Customer {
  id?: number;
  name: CustomerName;
  address: CustomerAddress;
  emails: Array<string>;
  phones: Array<string>;
}

export interface CustomerName {
  fullName: string;
  firstName: string;
  lastName: string;
}

export interface CustomerAddress {
  streetAddress: string;
  city: string;
  state: string;
  zipCode: string;
}
