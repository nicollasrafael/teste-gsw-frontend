import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerListComponent } from './customer-list/customer-list.component'
import { CustomerDetailsComponent } from './customer-details/customer-details.component'

const routes: Routes = [
  { path: 'customers', component: CustomerListComponent },
  { path: 'customers/new', component: CustomerDetailsComponent },
  { path: 'customers/edit/:customerId', component: CustomerDetailsComponent },
  { path: '',   redirectTo: '/customers', pathMatch: 'full' },
  { path: '**', redirectTo: "/customers" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
