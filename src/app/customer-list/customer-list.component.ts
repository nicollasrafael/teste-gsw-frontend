import { Component, OnInit } from '@angular/core';
import { CustomerService, Customer } from '../customer.service'
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  isLoaded: boolean;

  customers: Customer[];

  faPlus = faPlus;
  faPencilAlt = faPencilAlt;
  faTrash = faTrash;
  
  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    this.customerService
      .listCustomers()
      .subscribe(customers => {
        this.isLoaded = true;
        this.customers = customers;
      });
  }

  deleteCustomer(customerId: number) {
    var deletionConfirm = confirm(`Are you sure you want to delete Customer #${customerId}?`);
    if (deletionConfirm) {
      this.customerService
        .deleteCustomer(customerId)
        .subscribe(_ => {
          this.getCustomers();
        });
    }
  }
}
