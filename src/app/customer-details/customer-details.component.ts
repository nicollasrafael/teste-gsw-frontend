import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
  isEditing: boolean;
  editingCustomerId: number;
  isCustomerLoaded: boolean;

  faPlus = faPlus;
  faTrash = faTrash;

  newEmailAndPhoneForm = this.fb.group({
    newEmail: ['', [Validators.required, Validators.email]],
    newPhone: ['', [Validators.required, Validators.pattern("([0-9]{1,3})([0-9]{11,12})")]]
  });

  customerForm = this.fb.group({
    name: ['', Validators.required],
    address: this.fb.group({
      streetAddress: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.pattern("([0-9]{4,8})")]]
    }),
    emails: this.fb.array([]),
    phones: this.fb.array([])
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute, 
    private customerService: CustomerService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      var customerId = params.get('customerId');
      if (customerId != null) {
        this.isEditing = true;
        this.editingCustomerId = parseInt(customerId);
        this.getCustomer();
      }
    });
  }

  get newEmail() {
    return this.newEmailAndPhoneForm.get('newEmail') as FormControl;
  }

  get newPhone() {
    return this.newEmailAndPhoneForm.get('newPhone') as FormControl;
  }

  get emails() {
    return this.customerForm.get('emails') as FormArray;
  }

  get phones() {
    return this.customerForm.get('phones') as FormArray;
  }

  addEmail() {
    if (this.newEmail.valid) {
      this.emails.push(this.fb.control(this.newEmail.value));
      this.newEmail.setValue('');
    }
  }

  addPhone() {
    if (this.newPhone.valid) {
      this.phones.push(this.fb.control(this.newPhone.value));
      this.newPhone.setValue('');
    }
  }

  removeEmail(index: number) {
    this.emails.removeAt(index);
  }

  removePhone(index: number) {
    this.phones.removeAt(index);
  }

  getCustomer() {
    this.customerService
      .getCustomer(this.editingCustomerId)
      .subscribe(customer => {

        this.customerForm.get('name').setValue(customer.name.fullName);
        this.customerForm.get('address').setValue(customer.address);

        customer
          .emails
          .forEach(email => this.emails.push(this.fb.control(email)));

        customer
          .phones
          .forEach(phone => this.phones.push(this.fb.control(phone)));

        this.isCustomerLoaded = true;
      })
  }

  saveCustomer() {
    if (this.customerForm.valid) {
      if (this.isEditing) {
        this.customerService
          .updateCustomer(this.editingCustomerId, this.customerForm.value)
          .subscribe(_ => {
            alert(`Customer #${this.editingCustomerId} updated!`);
          });
      } else {
        this.customerService
          .createCustomer(this.customerForm.value)
          .subscribe(customer => {
            alert(`Customer #${customer.id} created!`);
            this.router.navigate(['/customers']);
          });
      }
    }
  }
}
