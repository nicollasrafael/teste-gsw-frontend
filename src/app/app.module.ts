import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxMaskModule } from 'ngx-mask';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerListComponent } from './customer-list/customer-list.component';

import { CustomerService } from './customer.service';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { TopBarComponent } from './top-bar/top-bar.component';

export let options: Partial<any> | (() => Partial<any>);

@NgModule({
  declarations: [
    AppComponent,
    CustomerListComponent,
    CustomerDetailsComponent,
    TopBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: [CustomerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
